package readability;
/**
 * @author nattapat sukpootanan
 * STATE 
 * */
public interface State {
	/**
	 * handle for handle char and set to another state 
	 * @param c is Character of each word.
	 * */	
	public void handle(char c);
}
