package readability;
/**
 * @author nattapat sukpootanan.
 * STATE DASH
 * */
public class DASH implements State{
	private WordCounter wordcounter;
	/**
	 * DASH Constructor.
	 * */
	public DASH(WordCounter wordcounter){
		this.wordcounter = wordcounter;
	}
	
	/**
	 * handle for handdle char and set to another state 
	 * @param c is Character of each word.
	 * */
	@Override 
	public void handle(char c) {
		if(wordcounter.isE_FIRST(c)){
			wordcounter.setState(wordcounter.E_FIRST);
		}
		else if(wordcounter.isLetter(c)&&!wordcounter.isVowel(c)){
			wordcounter.setState(wordcounter.CONSONANT);
			}
		else if(wordcounter.isVowel(c)||c=='y'||c=='Y'){ 
			wordcounter.setState(wordcounter.VOWEL);
		}
		else{ 
			wordcounter.setState(wordcounter.NONWORD);
		}
		
	}
	
	
}
