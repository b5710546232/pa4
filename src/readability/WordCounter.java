package readability;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Scanner;
import java.util.StringTokenizer;

/**
 * @author Nattapat Sukpootanan
 * WordCounter for count word and syllables.
 * */
public class WordCounter {
	private int syllables;
	public int sylablesForEachWord;
	public State state;
	public int lastIndexForEachWord;
	public int countIndexForEachWord;
	public int sentence;
	public int word;
	State START = new START(this);
	State CONSONANT = new CONSONANT(this);
	State VOWEL = new VOWEL(this);
	State E_FIRST = new E_FIRST(this);
	State DASH = new DASH(this);
	State NONWORD = new NONWORD(this);
	/**
	 * Constructor.
	 * */
	public WordCounter(){

	}
	/**
	 * ountSyllables count the Syllables of word
	 *@param word for count Syllables.
	 *@return syllables. 
	 * */
	public int countSyllables( String word ){
		this.sylablesForEachWord = 0; 
		this.lastIndexForEachWord = word.length()-1;
		this.countIndexForEachWord = 0;
		this.state = START;
		char [] array = word.toCharArray();
		for(char c: array ){
			state.handle(c);
			this.countIndexForEachWord++;
		}
		return this.sylablesForEachWord;

	}
	/**
	 * Checking it is Vowel.
	 * @param c is Character for checking.
	 * @return true if it is Vowel,false if it is not Vowel.
	 * */
	public boolean isVowel(char c){
		return "AEIOUaeiou".indexOf(c)>=0;
	}

	/**
	 * Checking it is Sentence.
	 * @param c is Character for checking.
	 * @return true if it is Vowel,false if it is not Vowel.
	 * */
	public boolean isSentence(char c){
		return ".;:?\n".indexOf(c)>=0;
	}
	/**
	 * checking in state E_FIRST is E or e.
	 * @param c is Character for checking.
	 * @return true is it is chracter e or E.
	 * */
	public boolean isE_FIRST(char c){
		return c=='E'||c=='e';
	}
	/**
	 *checking character that is Letter or not.
	 *@param c is Character for checking
	 *@return true if it is Letter , false if it is not.
	 * */
	public boolean isLetter(char c){
		return Character.isLetter(c);
	}
	/**
	 * Count all words read from an input stream an return the total count.
	 * @param instream is the input stream to read from.
	 * @return number of words in the InputStream. -1 if the stream
	 * cannot be read.
	 */
	public int countWords(InputStream instream) {
		this.syllables = 0; // reset
		BufferedReader breader = new BufferedReader(new InputStreamReader(instream));
		int word = 0;
		this.sentence = 0;
		int wordForEachSentence= 0;
		while( true ) {
			String line="";
			try {
				line = breader.readLine( );
			} catch (IOException e) {
				e.printStackTrace();
			}
			if (line == null) break;
			StringTokenizer token = new StringTokenizer(line);
			while(token.hasMoreTokens()){
				String s = token.nextToken();
				char [] array = s.toCharArray();
				if( this.countSyllables(s)!=0){
					word++;
					wordForEachSentence++;
				}
				for(char c:array)
				{
						if(this.isSentence(c)&&wordForEachSentence>0){
							this.sentence++;
							wordForEachSentence= 0;
						}
				}
				this.syllables += this.countSyllables(s);


				
			}
			
		}
		this.word =word;
		return this.word;
	}
	/**
	 *count Words in URL.
	 *@param url is address that want to counting words.s
	 *@return number of words
	 */
	public int countWords(URL url) {
		InputStream input = null;
		try {
			input = url.openStream();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return countWords(input);
	}
	/**
	 * getAll Syllables that counted.
	 * @return number all of Syllables. 
	 * */
	public int getSyllableCount() {

		return this.syllables;
	}
	public int getSentence(){
		return this.sentence;
	}
	public void setState(State state){
		this.state = state;
	}
	public boolean isLastChar(){
		return this.countIndexForEachWord == this.lastIndexForEachWord;
	}
	public double computeFleschIndex(){
		double fleschIndex = 206.835 - 84.6*(this.syllables/this.word) - 1.015*(this.word/this.sentence);
		return fleschIndex ;
	}
	public String getReadAbility(double fleschIndex){
		if(fleschIndex>100) return "4th grade student (elementary school) ";
		else if(fleschIndex>90&&fleschIndex<=100) 	return "5th grade student";
		else if(fleschIndex>80&&fleschIndex<=90)	return "6th grade student";
		else if(fleschIndex>70&&fleschIndex<=80)	return "7th grade student";
		else if(fleschIndex>65&&fleschIndex<=70)	return "8th grade student";
		else if(fleschIndex>60&&fleschIndex<=65)	return "9th grade student";
		else if(fleschIndex>50&&fleschIndex<=60)	return "High school student";
		else if(fleschIndex>30&&fleschIndex<=50)	return "College student";
		else if(fleschIndex>0&&fleschIndex<=30)		return "College graduate";
		else 										return "Advanced degree graduate";

	}
}
