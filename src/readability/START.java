package readability;
/**
 * @author nattapat sukpootanan.
 * START STATE FOR ENTER TO OTHER STATE
 * */
public class START implements State{
	private WordCounter wordcounter;
	/**
	 * START Constructor.
	 * @param wordcounter is got from WordCounter.
	 * */
	public  START(WordCounter wordcounter){
		this.wordcounter = wordcounter;

	}
	/**
	 * handle for handle char and set to another state 
	 * @param c is Character of each word.
	 * */
	@Override 
	public void handle(char c) {
//		this.wordcounter.countIndexForEachWord++; // count
		if(c=='-'&&this.wordcounter.countIndexForEachWord == 0){
			this.wordcounter.countIndexForEachWord = this.wordcounter.lastIndexForEachWord-1;
			wordcounter.setState(wordcounter.NONWORD);
		}
		else if((wordcounter.isVowel(c)||c=='y'||c=='Y')&&wordcounter.countIndexForEachWord == wordcounter.lastIndexForEachWord){
			wordcounter.sylablesForEachWord++;
		}
		else if(wordcounter.isLetter(c)&&!wordcounter.isVowel(c)){
			wordcounter.setState(wordcounter.CONSONANT);
		}
		else if(wordcounter.isE_FIRST(c)){
			wordcounter.setState(wordcounter.E_FIRST);
			
		}
		else if(wordcounter.isVowel(c)||c=='y'||c=='Y'){ 
			wordcounter.setState(wordcounter.VOWEL);
		}
		else if(!wordcounter.isLetter(c)){ 
			if(c=='-'){
				this.wordcounter.countIndexForEachWord = wordcounter.lastIndexForEachWord-1;
				wordcounter.setState(wordcounter.NONWORD);
			}
			else if(!(c=='\''||c=='('||c==')'||c=='.'||c=='\"'||c==','
					||c==';'||c=='?'||c=='!'||c=='/'||c==']'||c=='['||c==':')){
				this.wordcounter.countIndexForEachWord = wordcounter.lastIndexForEachWord-1;
				wordcounter.setState(wordcounter.NONWORD);
			}
			else{
				wordcounter.setState(wordcounter.NONWORD);
			}
		}
	}

}
