package readability;
/**
 * @author nattapat sukpootanan 
 * CONSONATANT state.
 * */
public class CONSONANT implements State{
	private WordCounter wordcounter;
	
	/**
	 * Constructor.
	 * @param wordcounter receive from WordCounter.
	 * */
	public CONSONANT(WordCounter wordcounter){
		this.wordcounter = wordcounter;
		
	}
/**
 * handle for handle char and set to another state 
 * @param c is Character of each word.
 * */
	@Override
	public void handle(char c) {
		
		if(wordcounter.isE_FIRST(c)){
			wordcounter.setState(wordcounter.E_FIRST);
			if(this.wordcounter.isLastChar()&&this.wordcounter.sylablesForEachWord==0){
				this.wordcounter.sylablesForEachWord++;
			}
			
		}
	
		else if(wordcounter.isVowel(c)||c=='y'||c=='Y') {
			wordcounter.setState(wordcounter.VOWEL);
			if(this.wordcounter.isLastChar()){
				this.wordcounter.sylablesForEachWord++;
			}
		}
		
		else if(c=='\''||wordcounter.isLetter(c)&&!wordcounter.isVowel(c)){
			wordcounter.setState(wordcounter.CONSONANT);
		}
		
		else if(c=='-'){
			wordcounter.setState(wordcounter.DASH);
			if(wordcounter.isLastChar()){
				this.wordcounter.sylablesForEachWord =0;
			}
		}
		else if(!wordcounter.isLetter(c)){
			wordcounter.setState(wordcounter.NONWORD);
		}
			
	}

}
