package readability;
/**
 * @author nattapat sukpootanan
 * E_FIRST STATE
 * */
public class E_FIRST implements State{
	private WordCounter wordcounter;
	/**
	 * E_FIRST Constructor.
	 * @param wordcounter is got from WordCounter.
	 * */
	public E_FIRST(WordCounter wordcounter){
		this.wordcounter = wordcounter;
	}

	/**
	 * handle for handle char and set to another state 
	 * @param c is Character of each word.
	 * */
	@Override 
	public void handle(char c) {
		if(this.wordcounter.isLastChar()&&this.wordcounter.sylablesForEachWord==0){
			
			wordcounter.sylablesForEachWord++;
		}
		else if(wordcounter.isLetter(c)&&!wordcounter.isVowel(c)){
			wordcounter.sylablesForEachWord++;//syllables++;
//			state = State.CONSONATE;
			wordcounter.setState(wordcounter.CONSONANT);
		}
		else if(wordcounter.isVowel(c)){
//			state = State.VOWEL;
			wordcounter.setState(wordcounter.VOWEL);
			
			//check last char is VOWEL
			if(wordcounter.isLastChar()){
				wordcounter.sylablesForEachWord++;
			}
		}
		else if(c=='-'){
			wordcounter.setState(wordcounter.DASH);
//			state = State.DASH;
			
			//check end with dash
			if(wordcounter.isLastChar()){
				this.wordcounter.sylablesForEachWord = 0;
			}
		}
		else if(!wordcounter.isLetter(c)){ 
			wordcounter.sylablesForEachWord++;
			wordcounter.setState(wordcounter.NONWORD);
//			state = State.NON_WORD;
		}
//		else {
////			return syllables;
//			this.wordcounter.sylablesForEachWord++;
//		}
	}

}
