package readability;
/**
 * @author nattapat sukpootanan.
 * NONWORD STATE.
 * */
 class NONWORD implements State{
	private WordCounter wordcounter;
	/**
	 * NONWORD Constructor.
	 * @param wordcounter is got from WordCounter.
	 * */
	public NONWORD(WordCounter wordcounter){
		this.wordcounter = wordcounter;
	}
	/**
	 * handle for handle char and set to another state 
	 * @param c is Character of each word.
	 * */
	@Override 

	public void handle(char c) {
		if(c=='\''||c=='('||c==')'||c=='.'||c=='\"'||c==','
				||c==';'||c=='?'||c=='!'||c=='/'||c==']'||c=='['||c==':'){
			// use for continue;			
		}
		else if(this.wordcounter.countIndexForEachWord == wordcounter.lastIndexForEachWord){
			this.wordcounter.countIndexForEachWord--;
			this.wordcounter.sylablesForEachWord = 0;
		}
		else if(wordcounter.isE_FIRST(c)){
			wordcounter.setState(wordcounter.E_FIRST);
		}
		else if(wordcounter.isVowel(c)||c=='y'||c=='Y') {
			wordcounter.setState(wordcounter.VOWEL);
		}
		else if(wordcounter.isLetter(c)&&!wordcounter.isVowel(c)){
			wordcounter.setState(wordcounter.CONSONANT);
		}
		else if(this.wordcounter.countIndexForEachWord == wordcounter.lastIndexForEachWord){
			this.wordcounter.countIndexForEachWord = wordcounter.lastIndexForEachWord;
			this.wordcounter.sylablesForEachWord = 0;
			wordcounter.setState(wordcounter.NONWORD);
		}
	}

}
