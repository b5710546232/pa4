package readability;
/**
 * @author nattapat sukpootanan
 * VOWEL STATE
 * */
public class VOWEL implements State {
	private WordCounter wordcounter;
	/**
	 * VOWEL Constructor.
	 * @param wordcounter is got from WordCounter.
	 * */

	public  VOWEL(WordCounter wordcounter){
		this.wordcounter = wordcounter;
	}
	/**
	 * handle for handle char and set to another state 
	 * @param c is Character of each word.
	 * */	

	public void handle(char c) {
		
		if(this.wordcounter.isLastChar()){
			wordcounter.sylablesForEachWord++;
			//check last char in DASH;
			if(c== '-'){
					wordcounter.sylablesForEachWord = 0;
			}
		}
		else if(c=='\''||wordcounter.isLetter(c)&&!wordcounter.isVowel(c)){
			wordcounter.setState(wordcounter.CONSONANT);
			
			wordcounter.sylablesForEachWord++;
		}
		else if(c=='-'){
			wordcounter.sylablesForEachWord++;
			wordcounter.setState(wordcounter.DASH);
		}
		else if(wordcounter.isVowel(c)){ 
			
			wordcounter.setState(wordcounter.VOWEL);
		}
		else if(!wordcounter.isLetter(c)){ 
			wordcounter.setState(wordcounter.CONSONANT);
		}
		else {
			wordcounter.sylablesForEachWord++;
		}
		
	}
}
