package readability;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.net.URL;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
/**
 * @author nattapat sukpootanan
 * FileCounter UI for Showing UI.
 * */
public class ReadabilityUI extends JFrame implements Runnable{
	private JTextField textURL = new JTextField(20);
	private JLabel label = new JLabel("File or URL name :");
	private JButton browserButton = new JButton("Browser...");
	private JButton countButton = new JButton("Count");
	private JButton clearButton = new JButton("Clear");
	private JTextArea textArea = new JTextArea(9,50);
	private WordCounter wordcounter;
	private String filePath = "";
	private URL url;
	/**
	 * Constructor of UI.
	 * @param wordcounter get from WordCounter.
	 * */
	public ReadabilityUI(WordCounter wordcounter){
		super("Readability by Nattapat Sukpootanan");
		this. wordcounter = wordcounter;
		this.initComponents( );
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	/**
	 * init all Components that use in  UI.
	 * */
	public void initComponents(){
		Container contents = this.getContentPane();
		LayoutManager flowLayout = new FlowLayout( );
		LayoutManager boxLayout = new BoxLayout(contents, BoxLayout.PAGE_AXIS);
		contents.setLayout(boxLayout);
		JPanel line1 = new JPanel();
		line1.setLayout(flowLayout);
		line1.add(label);
		line1.add(textURL);
		line1.add(browserButton);
		line1.add(countButton);
		line1.add(clearButton);
		contents.add(line1);
		
		JPanel line2 = new JPanel();
		line2.setLayout(flowLayout);
		textArea.setSize(getMaximumSize());
		JScrollPane scrolling = new JScrollPane(line2,
	            JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
	            JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrolling.setViewportView(textArea);
		line2.add(scrolling);
		contents.add(line2);
		initButtonList();
		
	}	
	/**
	 * add all of Action Listener to Button.
	 * */
	public void initButtonList(){
		browserButton.addActionListener(new ActionListener() {
			/**
			 * add ActionLister to browserButton.
			 * */
			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser browser = new JFileChooser();
				browser.showOpenDialog(null);
				File f = browser.getSelectedFile();
				try{
				filePath = f.getAbsolutePath();
				
				textURL.setText("file:"+filePath);
				}
				catch (Exception ecp){
					
				}
				
				
			}
		});
		countButton.addActionListener(new ActionListener(){
			/**
			 * add ActionLister to countButton.
			 * */
			@Override
			public void actionPerformed(ActionEvent e) {
				try{
//				textArea.setText(textToPrint());
					textArea.append(textToPrint());
				}
				catch(Exception ect){
				}
				
			}
		});
		/**
		 * add ActionLister to clearButton.
		 * */
		clearButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				textArea.setText("");
				
			}
		});
	}
	/**
	 * textToPrint get all information for showing in textArea.
	 * @return string of all information that computing.
	 * */
	public String textToPrint(){
		String URL = textURL.getText();
		try {
			 url = new URL( URL );
		} catch(Exception e){
			
		}
		int wordcount = wordcounter.countWords( url );
		int syllables = wordcounter.getSyllableCount( );
		int sentence = wordcounter.getSentence();
		double fleschIndex = wordcounter.computeFleschIndex();
		String readAbility = wordcounter.getReadAbility(fleschIndex);
		String s = "\nFilename: "+url.getPath()+"\nNumber of Syllables: "+syllables+"\n"
				+ "Number of Words: "+wordcount+"\nNumber of Sentences: "+sentence
				+ "\nFlesch Index: "+fleschIndex+"\nReadability: "+readAbility+"\n\n";
		return s;
		
	}
	/**
	 * running UI.
	 * */
	public void run(){
		this.pack();
		this.setVisible(true);
	}
}
