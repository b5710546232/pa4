package readability.Readability;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

import readability.ReadabilityUI;
import readability.WordCounter;
/**
 * @author nattapat sukpootanan
 * Main method for running ReadAbility.program
 * */
public class Readability {
	public static void main(String[] args) throws MalformedURLException {
		WordCounter counter = new WordCounter();
		if(args.length > 0){
			for (String arg : args)
				CommandLineMode(counter, arg);
		}
		else{
			UImode(counter);

		}
	}
	/**
	 * Method for use CommandLine Mode
	 * @param counter is WordCounter for counting .
	 * @param agr is for URL.
	 * */
	public static void CommandLineMode(WordCounter counter, String arg) throws MalformedURLException{
		Scanner input = new Scanner(System.in);
		String DICT_URL = arg;
		URL url = new URL( DICT_URL );

		int wordcount = counter.countWords( url );
		int syllables = counter.getSyllableCount( );
		int sentence = counter.getSentence();
		double fleschIndex = counter.computeFleschIndex();
		String readAbility = counter.getReadAbility(fleschIndex);
		String s = "\nFilename: "+url+"\nNumber of Syllables: "+syllables+"\n"
				+ "Number of Words: "+wordcount+"\nNumber of Sentences: "+sentence
				+ "\nFlesch Index: "+fleschIndex+"\nReadability: "+readAbility;
		System.out.println(s);

	}
	/**
	 * For running this program in UI mode.
	 * @param counter is WordCounter for counting 
	 * */
	public static void UImode(WordCounter wordcounter){
		ReadabilityUI ui = new ReadabilityUI(wordcounter);
		ui.run();
	}
}
